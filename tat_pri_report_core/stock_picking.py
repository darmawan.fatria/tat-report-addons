##############################################################################
#
#    Darmawan Fatriananda
#    BKD Pemprov Jabar
#    Copyright (c) 2016
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################


from openerp.osv import fields, osv
from openerp.tools.translate import _



class stock_picking(osv.Model):
    _inherit = 'stock.picking'


    def do_enter_transfer_details(self, cr, uid, picking, context=None):
        print "do_enter_transfer_details "
        return super(stock_picking, self).do_enter_transfer_details(cr,uid,picking,context=None)



stock_picking()
class stock_transfer_details(osv.Model):
    _inherit = 'stock.transfer_details'


    def do_detailed_transfer(self, cr, uid, picking, context=None):
        print "do_detailed_transfer "
        return super(stock_transfer_details, self).do_detailed_transfer(cr,uid,picking,context=None)



stock_transfer_details()

    