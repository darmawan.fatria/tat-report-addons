##############################################################################
#
#    Darmawan Fatriananda
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Picking List Atas",
    "version": "1.0",
    "author": "darmawan fatriananda",
    "category": "Warehouse /Reporting / ",
    "description": """
  Picking List Atas
    """,
    "website" : "http://-",
    "license" : "GPL-3",
    "depends": [
                "stock",
                ],
    'data': [
                   "picking_listpick_report_view.xml",
                   "report/picking_listpick_pdf_report_view.xml",
                   ],
    'installable': True,
    'active': False,

}