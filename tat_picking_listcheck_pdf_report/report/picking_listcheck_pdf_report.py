from datetime import datetime,timedelta
import time
from openerp.osv import osv
from openerp.report import report_sxw
import locale
from string import upper
from mx import DateTime

class report_picking_listcheck_report(report_sxw.rml_parse):
    def __init__(self, cr, uid, name, context=None):
        locale.setlocale(locale.LC_ALL, 'id_ID.utf8')
        super(report_picking_listcheck_report, self).__init__(cr, uid, name, context=context)
        self.localcontext.update({
            'get_detail_picking_listcheck' : self._get_detail_picking_listcheck,
            'get_format_date' : self._get_format_date,
            'get_current_date' : self._get_current_date,
            'to_upper':self._to_upper,
            'format_integer' :self.format_integer,
            'get_product_brand':self._get_product_brand,
           
        })
    def _get_detail_picking_listcheck(self,data):

        obj_data=self.pool.get('stock.picking').browse(self.cr,self.uid,data['data_ids'])

        return obj_data

    def _get_format_date(self,a_date):
        formatted_print_date=''
        try :
            formatted_print_date = time.strftime('%d %B %Y', time.strptime(a_date,'%Y-%m-%d'))
        except :
            formatted_print_date='-'
            
        return formatted_print_date
    def _get_current_date(self):
        return DateTime.today();
    def _to_upper(self,a_string):
        return a_string and upper(a_string) or ''
    def format_integer(self,val,context=None):
        try :
            return int(val)
        except:
            return None
    def _get_product_brand(self,obj_data):
        return obj_data and obj_data.product_id and obj_data.product_id.product_tmpl_id and obj_data.product_id.product_tmpl_id.product_brand_id and obj_data.product_id.product_tmpl_id.product_brand_id.name or ''
    

class wrapped_report_picking_listcheck(osv.AbstractModel):
    _name = 'report.tat_picking_listcheck_pdf_report.report_picking_listcheck_report'
    _inherit = 'report.abstract_report'
    _template = 'tat_picking_listcheck_pdf_report.report_picking_listcheck_report'
    _wrapped_report_class = report_picking_listcheck_report